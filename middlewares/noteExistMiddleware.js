const {Note} = require('../models/noteModel');

module.exports.noteExistMiddleware = async (req, res, next) => {
  const noteId = req.params.id;
  const {_id} = res.locals;
  const note = await Note.findOne({_id: noteId}, {__v: 0});

  if (_id !== note.userId) {
    return res.status(400).json({
      message: 'This account doesn\'t have note with this ID!',
    });
  }
  next();
};
