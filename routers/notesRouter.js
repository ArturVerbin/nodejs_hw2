const express = require('express');
const router = new express.Router();
const {asyncWrapper} = require('../helpers/helpers');
const {noteExistMiddleware} = require('../middlewares/noteExistMiddleware');
const {
  getNotes,
  createNote,
  getCurrentNote,
  updateCurrentNote,
  updateCompletedValue,
  deleteCurrentNote,
} = require('../controllers/notesController');
const {authMiddleware} = require('../middlewares/authMiddleware');

router.get('', asyncWrapper(authMiddleware), asyncWrapper(getNotes));
router.post('', asyncWrapper(authMiddleware), asyncWrapper(createNote));
router.get('/:id',
    asyncWrapper(authMiddleware),
    asyncWrapper(noteExistMiddleware),
    asyncWrapper(getCurrentNote),
);
router.put('/:id',
    asyncWrapper(authMiddleware),
    asyncWrapper(noteExistMiddleware),
    asyncWrapper(updateCurrentNote),
);
router.patch('/:id',
    asyncWrapper(authMiddleware),
    asyncWrapper(noteExistMiddleware),
    asyncWrapper(updateCompletedValue),
);
router.delete('/:id',
    asyncWrapper(authMiddleware),
    asyncWrapper(noteExistMiddleware),
    asyncWrapper(deleteCurrentNote),
);

module.exports = router;
