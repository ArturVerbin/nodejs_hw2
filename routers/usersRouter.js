const express = require('express');
const router = new express.Router();
const {asyncWrapper} = require('../helpers/helpers');
const {authMiddleware} = require('../middlewares/authMiddleware');
const {
  deleteUser,
  getUserInfo,
  updateUserPassword} = require('../controllers/usersController');

router.get(
    '/me',
    asyncWrapper(authMiddleware),
    asyncWrapper(getUserInfo),
);
router.delete(
    '/me',
    asyncWrapper(authMiddleware),
    asyncWrapper(deleteUser),
);
router.patch(
    '/me',
    asyncWrapper(authMiddleware),
    asyncWrapper(updateUserPassword),
);

module.exports = router;
