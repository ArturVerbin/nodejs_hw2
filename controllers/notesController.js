const {Note} = require('../models/noteModel');

module.exports.getNotes = async (req, res) => {
  const {_id} = res.locals;
  const {limit='5', offset='0'} = req.query;
  const notes = await Note.find(
      {userId: _id},
      {__v: 0},
      {
        limit: parseInt(limit) > 100 ? 5 : parseInt(limit),
        skip: parseInt(offset),
      },
  );

  res.json({notes});
};

module.exports.createNote = async (req, res) => {
  const {_id} = res.locals;
  const {text} = req.body;
  const note = new Note({
    userId: _id,
    text,
  });

  await note.save();

  res.status(200).json({message: 'Success'});
};

module.exports.getCurrentNote = async (req, res) => {
  const noteId = req.params.id;
  const note = await Note.findOne({_id: noteId}, {__v: 0});

  res.status(200).json({note});
};

module.exports.updateCurrentNote = async (req, res) => {
  const noteId = req.params.id;
  const {text} = req.body;

  await Note.findByIdAndUpdate(noteId, {text});

  res.status(200).json({message: 'Success'});
};

module.exports.updateCompletedValue = async (req, res) => {
  const noteId = req.params.id;
  const note = await Note.findById(noteId);

  note.completed = !note.completed;

  await note.save();

  res.status(200).json({message: 'Success'});
};

module.exports.deleteCurrentNote = async (req, res) => {
  const noteId = req.params.id;

  await Note.findByIdAndDelete(noteId);

  res.status(200).json({message: 'Success'});
};
