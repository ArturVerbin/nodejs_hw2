const {User} = require('../models/userModel');
const bcrypt = require('bcrypt');
const saltRounds = 10;

module.exports.deleteUser = async (req, res) => {
  const {_id} = res.locals;
  await User.findByIdAndDelete({_id});
  res.status(200).json({message: 'Success'});
};

module.exports.getUserInfo = async (req, res) => {
  const {_id} = res.locals;
  const user = await User.findById(_id);
  const userData = {
    'user': {
      _id: user._id,
      username: user.username,
      createdDate: user.createdDate,
    },
  };

  res.status(200).json(userData);
};

module.exports.updateUserPassword = async (req, res) => {
  const {_id} = res.locals;
  const {oldPassword, newPassword} = req.body;
  const user = await User.findById(_id);
  if (!(await bcrypt.compare(oldPassword, user.password))) {
    return res.status(400).json({message: 'Wrong password!'});
  }

  await User.findOneAndUpdate(
      {_id},
      {password: await bcrypt.hash(newPassword, saltRounds)},
  );

  res.status(200).json({message: 'Success'});
};
