require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const authRouter = require('./routers/authRouter');
const usersRouter = require('./routers/usersRouter');
const notesRouter = require('./routers/notesRouter');
const {PORT, DB_HOSTNAME, DB_NAME, DB_PASS, DB_USER} = require('./config');
const app = express();

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth/', authRouter);
app.use('/api/users/', usersRouter);
app.use('/api/notes/', notesRouter);

app.use((err,
    req,
    res,
    next) => {
  res.status(500).json({message: err.message});
});

const start = async () => {
  await mongoose.connect(`mongodb+srv://${DB_USER}:${DB_PASS}@${DB_HOSTNAME}/${DB_NAME}?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  app.listen(PORT, ()=> console.log(`Server works at ${PORT} port!`));
};

start();
